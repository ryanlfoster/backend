<?php

return array(
    'boards'          => [
        'path'     => '/boards',
        'defaults' => [
            '_controller' => 'board.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'collection',
                'class' => 'BoardTransformer'
            ],
        ]
    ],

    'board.configure' => [
        'path'     => '/boards/configure',
        'defaults' => [
            '_controller' => 'board.controller:configureAction'
        ],
        'methods'  => [
            'POST'
        ],
    ],
);