<?php

return array(
    'comments'     => [
        'path'     => '/comments',
        'defaults' => [
            '_controller' => 'comment.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Collection',
                'class' => 'CommentTransformer'
            ],
        ]
    ],

    'comment.create'     => [
        'path'     => '/comments',
        'defaults' => [
            '_controller' => 'comment.controller:createAction'
        ],
        'methods'  => [
            'POST'
        ],
        'options' => [
            //'response.type' => 'ws',
            'response.type' => 'json',
            'transformer' => [
                'type' => 'item',
                'class' => 'CommentTransformer'
            ],
        ]
    ],

    'comment.update'     => [
        'path'     => '/comments',
        'defaults' => [
            '_controller' => 'comment.controller:updateAction'
        ],
        'methods'  => [
            'PUT'
        ],
        'options' => [
            //'response.type' => 'ws',
            'response.type' => 'json',
            'transformer' => [
                'type' => 'item',
                'class' => 'CommentTransformer'
            ],
        ]
    ],
);
