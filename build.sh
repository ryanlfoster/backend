#!/bin/sh
set -e

#composer update --no-dev

echo "env[GITLAB_HOST]=$GITLAB_HOST" >> /etc/php/php-fpm.conf
echo "env[GITLAB_OAUTH_CLIENT_ID]=$GITLAB_OAUTH_CLIENT_ID" >> /etc/php/php-fpm.conf
echo "env[GITLAB_OAUTH_CLIENT_SECRET]=$GITLAB_OAUTH_CLIENT_SECRET" >> /etc/php/php-fpm.conf
echo "env[GITLAB_API_TOKEN]=$GITLAB_API_TOKEN" >> /etc/php/php-fpm.conf
echo "env[GITLAB_BASIC_LOGIN]=$GITLAB_BASIC_LOGIN" >> /etc/php/php-fpm.conf
echo "env[GITLAB_BASIC_PASSWORD]=$GITLAB_BASIC_PASSWORD" >> /etc/php/php-fpm.conf
echo "env[KANBAN_SECRET_KEY]=$KANBAN_SECRET_KEY" >> /etc/php/php-fpm.conf

exec "$@"
