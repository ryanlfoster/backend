FROM leanlabs/php:latest

EXPOSE 9000

RUN sed -i 's/\;date\.timezone\ \=/date\.timezone\ \=\ Europe\/Moscow/g' /etc/php/php.ini \
    && sed -i 's/listen = 127.0.0.1:9000/listen = 0.0.0.0:9000/g' /etc/php/php-fpm.conf \
    && sed -i 's/listen.allowed_clients = 127.0.0.1/;listen.allowed_clients = 127.0.0.1/g' /etc/php/php-fpm.conf

COPY . /var/www/backend
WORKDIR /var/www/backend

ENTRYPOINT ["/bin/sh", "/var/www/backend/build.sh"]

CMD ["php-fpm", "-F"]
