<?php

namespace Kanban\Transformer;

use Gitlab\Models\Issue;
use Gitlab\Models\Milestone;
use League\Fractal\TransformerAbstract;

class CardTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'milestone',
    ];

    public function transform(Issue $card)
    {
        $result = [
            'id'         => $card->getId(),
            'iid'        => $card->getIid(),
            'title'      => $card->getTitle(),
            'project_id' => $card->getProjectId(),
            'labels'     => $card->getLabels(),
            'description'=> $this->todoClear($card->getDescription()),
            'author'     => $card->getAuthor(),
            'assignee'   => $card->getAssignee(),
            'state'      => $card->getState(),
            'todo'       => $this->includeTodo($card->getDescription())
        ];

        return $result;
    }

    public function todoClear($desc)
    {
        return preg_replace('|[-\*]{1} (\[.\])(.*)|','', $desc);
    }

    public function includeTodo($description)
    {
        $matches = [];
        if (preg_match_all('|[-\*]{1} (\[.\])(.*)|', $description, $matches)) {

            $todoList = [];

            foreach ($matches[0] as $k => $item) {
                $todoList[] = [
                    'checked' => $matches[1][$k] == '[x]' ? true : false,
                    'body'  => trim($matches[2][$k])
                ];
            }

            return $todoList;
        }

        return [];
    }

    /**
     * Include Milestone
     *
     * @param Issue $card
     * @return \League\Fractal\Resource\Item
     */
    public function includeMilestone(Issue $card)
    {
        $milestone = $card->getMilestone();

        if ($milestone instanceOf Milestone) {
            $milestone = $this->item($milestone, new MilestoneTransformer());
        }

        return $milestone;
    }
}