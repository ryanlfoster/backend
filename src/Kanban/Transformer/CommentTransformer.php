<?php

namespace Kanban\Transformer;

use Gitlab\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    public function transform(Comment $comment)
    {
        $result = [
            'id' => $comment->getId(),
            'author' => $comment->getAuthor(),
            'body' => $comment->getBody(),
            'created_at' => $comment->getCreatedAt()->getTimestamp(),
        ];

        return $result;
    }
}

