<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CardController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $projectId = $request->query->get('project_id', 0);
        $state = $request->query->get('state', 'opened');
        $per_page = $request->query->get('per_page', 50);

        $response  = $this->app['gitlab_api']->executeCommand('GetIssuesByProject',
            ['project_id' => $projectId,
             'state' => $state,
             'per_page' =>$per_page]
        );

        return $response;
    }

    public function cardAction(Request $request)
    {
        $project_id = $request->query->get('project_id', 0);
        $issue_id   = $request->query->get('issue_id', 0);

        $response   = $this->app['gitlab_api']->executeCommand('GetIssue', [
            'project_id' => $project_id,
            'issue_id'   => $issue_id
        ]);

        return $response;
    }

    public function createAction(Request $request)
    {
        $response   = $this->app['gitlab_api']->executeCommand('CreateIssue', $request->request->all());

        return $response;
    }

    public function updateAction(Request $request)
    {
        $content = $request->request->all();

        if (! empty($content['description'])) {
            $content['description'] .= $this->encodeTodo($content['todo']);
        }

        $response = $this->app['gitlab_api']->executeCommand('EditIssue', $content);

        return $response;
    }

    public function deleteAction(Request $request)
    {
        $vars = $request->request->all();

        $vars['state_event'] = 'close';
        $response = $this->app['gitlab_api']->executeCommand('DeleteIssue', $vars);

        return $response;
    }

    /**
    * Преобразование из списка todo в описание тикета
    */
    protected function encodeTodo($todoList)
    {
        $result = '';

        foreach ($todoList as $todo) {
            $result .= '- ['.($todo['checked'] ? 'x' : ' ').'] '. $todo['body'].PHP_EOL;
        }

        return $result;
    }
}