<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;

class BoardController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $per_page = $request->query->get("per_page", 1000);

        $response = $this->app['gitlab_api']->executeCommand('GetProjects', ['per_page' => $per_page]);

        return $response;
    }

    public function configureAction(Request $request)
    {
        $vars = $request->request->all();

        $project_id = $vars['project_id'];

        $labels = [
            'KB[stage][0][Backlog]',
            'KB[stage][1][Development]',
            'KB[stage][2][Testing]',
            'KB[stage][3][Production]',
            'KB[stage][4][Ready]',
        ];

        foreach ($labels as $label) {
            $this->app['gitlab_api']->executeCommand('CreateLabel', ['project_id' => $project_id, 'name' => $label, 'color' => '#F5F5F5']);
        }

        return $this->app->json(['success'=>true]);
    }
}
