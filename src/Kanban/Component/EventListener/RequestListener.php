<?php

namespace Kanban\Component\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestListener implements EventSubscriberInterface
{

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $vars = $request->getContent();


        if ('json' === $request->getContentType()) {
            if (! empty($vars)) {
                $vars = json_decode($vars, true);

                foreach ($vars as $key => $value) {
                    $request->request->set($key, $value);
                }
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => 'onKernelRequest'
        );
    }
}