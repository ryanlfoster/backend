### Installation

```
docker build -t leanlabs/backend .
```

### Usage in development environment

```
docker run -i -v /path/to/source:/var/www/backend -d -t leanlabs/backend
```
